import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ElementUI from 'element-ui'


Vue.use(VueRouter)
Vue.use(ElementUI)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/edittest',
    name: 'Edittest',
    component: () => import(/* webpackChunkName: "about" */ '@/views/CkEditorEx.vue')

  },
  {
    path: '/cmponenttest',
    name: 'Cmponenttest',
    component: () => import(/* webpackChunkName: "about" */ '@/views/CmponentTest.vue')

  },
  {
    path: '/elementui',
    name: 'Elementui',
    component: () => import(/* webpackChunkName: "about" */ '@/views/ElementUiEx.vue')

  },
  

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
